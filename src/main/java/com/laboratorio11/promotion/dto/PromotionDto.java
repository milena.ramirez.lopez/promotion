package com.laboratorio11.promotion.dto;

import lombok.Data;

@Data
public class PromotionDto {

    private Integer totalSent;
}
