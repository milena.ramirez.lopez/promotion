package com.laboratorio11.promotion.controller;

import com.laboratorio11.promotion.dto.PromotionDto;
import com.laboratorio11.promotion.model.Client;
import com.laboratorio11.promotion.service.ClientService;
import com.laboratorio11.promotion.service.PromotionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/promotions")
public class PromotionController {

    private PromotionService promotionService;

    public PromotionController(PromotionService promotionService) {
        this.promotionService = promotionService;
    }

    @GetMapping(value = "/birthdate")
    public PromotionDto getClientsBirthayTodaySendPromotion () {
        PromotionDto promotionDto = new PromotionDto();

        promotionDto.setTotalSent(promotionService.sendBirthdayEmailOffers());

        return promotionDto;

    }
}
