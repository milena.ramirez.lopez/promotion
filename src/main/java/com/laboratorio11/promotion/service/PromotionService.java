package com.laboratorio11.promotion.service;

import com.laboratorio11.promotion.mail.EmailService;
import com.laboratorio11.promotion.model.Client;
import com.laboratorio11.promotion.model.Promotion;
import com.laboratorio11.promotion.repository.PromotionRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
public class PromotionService {

    @Value("${promotion.discount.value}")
    private Integer discountValue;

    private PromotionRepository promotionRepository;
    private ClientService clientService;
    private EmailService emailService;

    public PromotionService(PromotionRepository promotionRepository, ClientService clientService, EmailService emailService) {
        this.promotionRepository = promotionRepository;
        this.clientService = clientService;
        this.emailService = emailService;
    }

    public void savePromotion(Promotion thePromotion) {

        Promotion promotion = new Promotion();
        promotion.setDecription(thePromotion.getDecription());
        promotion.setCreatedAt(thePromotion.getCreatedAt());

        promotionRepository.save(promotion);
    }

    public List<Promotion> getPromotions() {

        List<Promotion> promotions = promotionRepository.findAll();

        return promotions;
    }

    public Promotion getPromotionById(Long id) {

        Promotion promotion = promotionRepository.getPromotionById(id);

        return promotion;
    }

    public Integer sendBirthdayEmailOffers() {
        String subjectEmail = "Promocion con un super descuento!!";

        // Obtener cumpleañeros del dia
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int month = localDate.getMonthValue();
        int day   = localDate.getDayOfMonth();
        List<Client> clients = clientService.getClientsByMonthAndDay(month, day);
        Promotion promotion = promotionRepository.getPromotionById(1L);
        String theText = promotion.getDecription();
        String promotionText = theText.replace("<discountValue>", discountValue.toString());

        String toEmail = "";
        for(Client client: clients) {
            toEmail = client.getEmail();
            String name = client.getFirstName() + " " + client.getLastName();
            String textEmail = promotionText.replace("<name>", name);

            emailService.sendSimpleMessage(toEmail, subjectEmail, textEmail);
        }

        return clients.size();
    }
}
