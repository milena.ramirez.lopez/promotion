package com.laboratorio11.promotion.service;

import com.laboratorio11.promotion.model.Client;
import com.laboratorio11.promotion.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    public void saveClient(Client theClient) {

        Client client = new Client();
        client.setFirstName(theClient.getFirstName());
        client.setLastName(theClient.getLastName());
        client.setEmail(theClient.getEmail());
        client.setBirthday(theClient.getBirthday());
        client.setCreatedAt(theClient.getCreatedAt());

        clientRepository.save(client);
    }

    public List<Client> getClientesDB() {

        List<Client> clients = clientRepository.findAll();

        return clients;
    }

    public List<Client> getClientsByMonthAndDay(Integer month, Integer day) {

        List<Client> clients = clientRepository.getClientsByMonthAndDay(month, day);

        return clients;

    }

}
