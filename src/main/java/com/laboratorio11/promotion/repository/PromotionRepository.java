package com.laboratorio11.promotion.repository;

import com.laboratorio11.promotion.model.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PromotionRepository extends JpaRepository<Promotion, Long> {
    Promotion getPromotionById(@Param("Id") Long id);
}
