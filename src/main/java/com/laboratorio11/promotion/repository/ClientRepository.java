package com.laboratorio11.promotion.repository;

import com.laboratorio11.promotion.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
     Client getClientById(Long id);

     @Query("from Client c where c.bithdayMonth = :month and c.bithdayDay = :day")
     List<Client> getClientsByMonthAndDay(@Param("month") Integer month, @Param("day") Integer day);
}
