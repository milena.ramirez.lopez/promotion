package com.laboratorio11.promotion.mail;

public interface EmailService {

    public void sendSimpleMessage(String to, String subject, String text);
}
