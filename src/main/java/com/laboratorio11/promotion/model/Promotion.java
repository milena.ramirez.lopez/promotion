package com.laboratorio11.promotion.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class Promotion extends BaseModel {

    @Column(nullable = false)
    private String decription;
}
