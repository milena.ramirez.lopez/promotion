package com.laboratorio11.promotion.model;

import lombok.Data;
import org.hibernate.annotations.Formula;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@Data
public class Client extends BaseModel {
    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private Date birthday;

    @Formula("MONTH(birthday)")
    private Integer bithdayMonth;

    @Formula("DAY(birthday)")
    private Integer bithdayDay;
}