package com.laboratorio11.promotion.mail;

import com.laboratorio11.promotion.model.Client;
import com.laboratorio11.promotion.model.Promotion;
import com.laboratorio11.promotion.service.ClientService;
import com.laboratorio11.promotion.service.PromotionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest()
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EmailServiceImplTest {

    @Autowired
    EmailService emailService;

    @Autowired
    ClientService clientService;

    @Autowired
    PromotionService promotionService;

    @Value("${promotion.discount.value}")
    private Integer discountValue;

    @Test
    void SendEmailPromotion() {

        String subjectEmail = "Promocion con un super descuento!!";

        // Obtener cumpleañeros del dia
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int month = localDate.getMonthValue();
        int day   = localDate.getDayOfMonth();
        List<Client> clients = clientService.getClientsByMonthAndDay(month, day);

        // Obtener Promocion
        Promotion promotion = promotionService.getPromotionById(1L);
        String promotionText = promotion.getDecription().replace("<discountValue>", discountValue.toString());

        String toEmail = "";
        for(Client client: clients) {
            toEmail = client.getEmail();
            String name = client.getFirstName() + " " + client.getLastName();
            String textEmail = promotionText.replace("<name>", name);

            System.out.println(name);
            System.out.println(discountValue);
            System.out.println(textEmail);
            System.out.println("");

            emailService.sendSimpleMessage(toEmail, subjectEmail, textEmail);
        }


    }
}