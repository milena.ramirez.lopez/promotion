package com.laboratorio11.promotion.service;

import com.laboratorio11.promotion.model.Client;
import com.laboratorio11.promotion.model.Promotion;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest()
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PromotionServiceTest {

    @Autowired
    PromotionService promotionService;

    @Test
    void ListarPromociones() {
        List<Promotion> promotions = promotionService.getPromotions();

        for (Promotion thePromo : promotions) {
            System.out.println(thePromo.getDecription());
        }

        assertNotNull(promotions);
    }

    @Test
    void sendBirthdayEmails () {

        Integer count = promotionService.sendBirthdayEmailOffers();

        System.out.println("Los cumpleañeros son: " + count);

        assertEquals(2, count);
    }

}