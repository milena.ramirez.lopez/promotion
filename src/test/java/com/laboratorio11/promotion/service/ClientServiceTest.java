package com.laboratorio11.promotion.service;

import com.laboratorio11.promotion.model.Client;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest()
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ClientServiceTest {

    @Autowired
    ClientService clientService;

    @Test
    void createClient() {

        Client client = new Client();
        client.setFirstName("Alessandra");
        client.setLastName("Fernandez Duran");
        client.setEmail("alessandra@gmail.com");

        client.setBirthday(new Date());
        client.setCreatedAt(new Date());

        System.out.println("Creando nuevo usuario ...");
        clientService.saveClient(client);
        System.out.println(client);

        assertNotNull(client);
        assertEquals("Alessandra", client.getFirstName());
    }

    @Test
    void listaClientes() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M", Locale.ENGLISH);
        List<Client> clients = clientService.getClientesDB();

        for (Client theClient : clients) {
            System.out.println(theClient.getFirstName() + " " + theClient.getLastName() + " => Birthday= " + formatter.format(theClient.getBirthday()));
        }

        assertNotNull(clients);
    }

    @Test
    void getClientByMonthAndDay() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M", Locale.ENGLISH);

        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int month = localDate.getMonthValue();
        int day   = localDate.getDayOfMonth();

        List<Client> clients = clientService.getClientsByMonthAndDay(month, day);

        for(Client theClient: clients) {
            System.out.println(theClient.getFirstName() + " " + theClient.getLastName() + " => Birthday= " + formatter.format(theClient.getBirthday()));
        }

        assertNotNull(clients);
    }
}