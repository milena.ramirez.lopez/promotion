package com.laboratorio11.promotion.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PromotionControllerTest {

    @Autowired
    private PromotionController promotionController;

    @Test
    public void testWebTestClientWithPromotionController() {
        WebTestClient.bindToController(promotionController)
                .build()
                .mutate()
                .responseTimeout(Duration.ofMillis(20000))
                .build()
                .get()
                .uri("/promotions/birthdate")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("totalSent")
                .isEqualTo(2);
    }

}