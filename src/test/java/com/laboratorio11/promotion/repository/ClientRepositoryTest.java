package com.laboratorio11.promotion.repository;

import com.laboratorio11.promotion.model.Client;
import com.laboratorio11.promotion.service.ClientService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ClientRepositoryTest {

    @Autowired
    ClientRepository clientRepository;

    @Test
    void createClient() {

        Client client = new Client();
        client.setFirstName("Milena");
        client.setLastName("Ramirez");
        client.setEmail("milena.ramirez.lopez@gmail.com");
        client.setBirthday(new Date());
        client.setCreatedAt(new Date());

        System.out.println("Creando cliente ...");
        clientRepository.save(client);

        System.out.println("Recuperando cliente ...");
        Client dbClient =  clientRepository.getClientById(client.getId());
        System.out.println(dbClient);

        assertNotNull(dbClient);
        assertEquals("Milena", dbClient.getFirstName());

    }

    @Test
    void getClientByMonth() {
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int month = localDate.getMonthValue();
        int day   = localDate.getDayOfMonth();

        List<Client> clients = clientRepository.getClientsByMonthAndDay(month, day);

        for(Client client: clients) {
            System.out.println(client);
        }

        assertNotNull(clients);
    }
}