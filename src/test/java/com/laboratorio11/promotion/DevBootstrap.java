package com.laboratorio11.promotion;

import com.laboratorio11.promotion.model.Client;
import com.laboratorio11.promotion.model.Promotion;
import com.laboratorio11.promotion.repository.ClientRepository;
import com.laboratorio11.promotion.repository.PromotionRepository;
import com.laboratorio11.promotion.service.ClientService;
import com.laboratorio11.promotion.service.PromotionService;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private ClientRepository clientRepository;
    private PromotionRepository promotionRepository;
    private final ClientService clientService;
    private final PromotionService promotionService;


    public DevBootstrap(ClientRepository clientRepository, PromotionRepository promotionRepository,
                        ClientService clientService, PromotionService promotionService) {
        this.clientRepository = clientRepository;
        this.promotionRepository = promotionRepository;
        this.clientService = clientService;
        this.promotionService = promotionService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        createClients();
        createPromotion();
    }

    private  void createClients() {

        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int month = localDate.getMonthValue();
        int day   = localDate.getDayOfMonth();

        String dateInString1 = day + "-"+ month + "-1982";
        Date date1 = getDate(dateInString1);
        String dateInString2 = "08-12-1991";
        Date date2 = getDate(dateInString2);
        String dateInString3 = "08-04-1983";
        Date date3 = getDate(dateInString3);
        String dateInString4 = "13-03-1986";
        Date date4 = getDate(dateInString4);
        String dateInString5 = "20-01-1979";
        Date date5 = getDate(dateInString5);
        String dateInString6 = "08-06-1985";
        Date date6 = getDate(dateInString6);
        String dateInString7 = "05-10-1987";
        Date date7 = getDate(dateInString7);
        String dateInString8 = day + "-"+ month + "-1993";
        Date date8 = getDate(dateInString8);


        Client client1 = createClient("Milena", "Ramirez Lopez", "milena.ramirez.lopez@gmail.com", date1, new Date());
        Client client2 = createClient("Melissa", "Rojas Ramirez", "melissa@gmail.com", date2, new Date());
        Client client3 = createClient("Javier", "Rojas Blum", "javier.rojas.blum@gmail.com", date3, new Date());
        Client client4 = createClient("Mariana", "Vega Perez", "mariana@gmail.com", date4, new Date());
        Client client5 = createClient("Roberto", "Oquendo Suarez", "roberto@gmail.com", date5, new Date());
        Client client6 = createClient("Gabriela", "Mejia Ore", "gabriela@gmail.com", date6, new Date());
        Client client7 = createClient("Patricia", "Guzman Flores", "patty@gmail.com", date7, new Date());
        Client client8 = createClient("Jorge", "Torrico Hernandez", "milena.ramirez.lopez+jorge@gmail.com", date8, new Date());


        clientService.saveClient(client1);
        clientService.saveClient(client2);
        clientService.saveClient(client3);
        clientService.saveClient(client4);
        clientService.saveClient(client5);
        clientService.saveClient(client6);
        clientService.saveClient(client7);
        clientService.saveClient(client8);
    }

    private Date getDate(String dateInString) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy", Locale.ENGLISH);

        Date date = new Date();
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private Client createClient(String firstName, String lastName, String email, Date birthday, Date createdAt) {
        Client client = new Client();
        client.setFirstName(firstName);
        client.setLastName(lastName);
        client.setEmail(email);
        client.setBirthday(birthday);
        client.setCreatedAt(createdAt);

        return client;
    }

    private void createPromotion() {

        String descripconPromocion = "<name> Hoy es su cumpleaños y usted es importante para nosotros, queremos\n" +
                                    "celebralo ofreciendo un <discountValue> % de descuento y delivery gratuito. Valido por 24 hrs";

        Promotion promo = new Promotion();
        promo.setDecription(descripconPromocion);
        promo.setCreatedAt(new Date());

        promotionService.savePromotion(promo);
    }
}
